##### << kcc---kanji code converter >> ####
#
#   Makefile
#                                                   Aug 24 1992
#                                       mod:        Feb 03 1994
################################################### tonooka ############

#	@(#)Makefile 2.3 (Y.Tonooka) 3/28/94

          BINPATH = /usr/local/bin
          MANPATH = /usr/man
          JMANDIR = ja_JP.ujis
           CFLAGS = -O

            SHELL = /bin/sh
               CP = cp
             MAKE = make
          INSTALL = install

             CMDS = kcc
             SRCS = kcc.c
             OBJS = kcc.o

all:	kcc

kcc:	kcc.o
	$(CC) $(CFLAGS) -o kcc kcc.o

install: $(BINPATH)/kcc
	@echo "\`make install' done."
	@echo "Run \`make install.man' to install a manual."

install.man: $(MANPATH)/$(JMANDIR)/man1/kcc.1

$(BINPATH)/kcc: kcc.c
	$(MAKE) kcc
	$(INSTALL) -s kcc $(BINPATH)

$(MANPATH)/$(JMANDIR)/man1/kcc.1: kcc.jman
	$(INSTALL) -m 644 kcc.jman $@

clean:
	rm -f $(CMDS) $(OBJS) mon.out gmon.out make.log

lint:   $(SRCS)
	$(LINT) $(LINTFLAGS) $(SRCS)
